import React from 'react'

import Card from '../../components/card'
import FormGroup from '../../components/form-group'
import SelectMenu from '../../components/selectMenu'

import { withRouter } from 'react-router-dom'
import * as messages from '../../components/toastr'

import LancamentoService from '../../app/service/appointmentService'
import LocalStorageService from '../../app/service/localstorageService'

class CadastroLancamentos extends React.Component {

    state = {
        id: null,
        description: '',
        value: '',
        month: '',
        year: '',
        type: '',
        status: '',
        user: null,
        updating: false
    }

    constructor(){
        super();
        this.service = new LancamentoService();
    }

    componentDidMount(){
        const params = this.props.match.params
       
        if(params.id){
            this.service
                .getById(params.id)
                .then(response => {
                    this.setState( {...response.data, updating: true} )
                })
                .catch(erros => {
                    messages.errorMessage(erros.response.data)
                })
        }
    }

    submit = () => {
        const loggedUser = LocalStorageService.adquireItem('_LOGGED_USER')

        const { description, value, month, year, type } = this.state;
        const lancamento = { description, value, month, year, type, user: loggedUser.id };

        try{
            this.service.validate(lancamento)
        }catch(erro){
            const mensagens = erro.mensagens;
            mensagens.forEach(msg => messages.errorMessage(msg));
            return false;
        }     

        const appointmentToSave = { 
            description, 
            value, 
            month, 
            year, 
            type, 
            user: { id: loggedUser.id },
            user_id: loggedUser.id
        };

        this.service
            .save(appointmentToSave)
            .then(response => {
                this.props.history.push('/get-appointment')
                messages.successMessage('Lançamento cadastrado com sucesso!')
            }).catch(error => {
                messages.errorMessage(error.response.data)
            })
    }

    update = () => {
        const { description, value, month, year, type, status, user, id } = this.state;

        const appointment = { 
            description, 
            value, 
            month, 
            year, 
            type, 
            user, 
            status, 
            id };
        
        this.service
            .update(appointment)
            .then(response => {
                this.props.history.push('/get-appointment')
                messages.successMessage('Lançamento atualizado com sucesso!')
            }).catch(error => {
                messages.errorMessage(error.response.data)
            })
    }

    handleChange = (event) => {
        const value = event.target.value;
        const name = event.target.name;

        this.setState({ [name] : value })
    }

    render(){
        const types = this.service.getTypeList();
        const months = this.service.getMonthList();

        return (
            <Card title={ this.state.updating ? 'Atualização de Lançamento'  : 'Cadastro de Lançamento' }>
                <div className="row">
                    <div className="col-md-12">
                        <FormGroup id="inputDescricao" label="Descrição: *" >
                            <input id="inputDescricao" type="text" 
                                   className="form-control" 
                                   name="description"
                                   value={this.state.description}
                                   onChange={this.handleChange}  />
                        </FormGroup>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-6">
                        <FormGroup id="inputAno" label="Ano: *">
                            <input id="inputAno" 
                                   type="text"
                                   name="year"
                                   value={this.state.year}
                                   onChange={this.handleChange} 
                                   className="form-control" />
                        </FormGroup>
                    </div>
                    <div className="col-md-6">
                        <FormGroup id="inputMes" label="Mês: *">
                            <SelectMenu id="inputMes" 
                                        value={this.state.month}
                                        onChange={this.handleChange}
                                        lista={months} 
                                        name="month"
                                        className="form-control" />
                        </FormGroup>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-4">
                         <FormGroup id="inputValor" label="Valor: *">
                            <input id="inputValor" 
                                   type="text"
                                   name="value"
                                   value={this.state.value}
                                   onChange={this.handleChange} 
                                   className="form-control" />
                        </FormGroup>
                    </div>

                    <div className="col-md-4">
                         <FormGroup id="inputTipo" label="Tipo: *">
                            <SelectMenu id="inputTipo" 
                                        lista={types} 
                                        name="type"
                                        value={this.state.type}
                                        onChange={this.handleChange}
                                        className="form-control" />
                        </FormGroup>
                    </div>

                    <div className="col-md-4">
                         <FormGroup id="inputStatus" label="Status: ">
                            <input type="text" 
                                   className="form-control" 
                                   name="status"
                                   value={this.state.status}
                                   disabled />
                        </FormGroup>
                    </div>

                   
                </div>
                <div className="row">
                     <div className="col-md-6" >
                        { this.state.updating ? 
                            (
                                <button onClick={this.update} 
                                        className="btn btn-success">
                                        <i className="pi pi-refresh"></i> Atualizar
                                </button>
                            ) : (
                                <button onClick={this.submit} 
                                        className="btn btn-success">
                                        <i className="pi pi-save"></i> Salvar
                                </button>
                            )
                        }
                        <button onClick={e => this.props.history.push('/get-appointment')} 
                                className="btn btn-danger">
                                <i className="pi pi-times"></i>Cancelar
                        </button>
                    </div>
                </div>
            </Card>
        )
    }
}

export default withRouter(CadastroLancamentos);