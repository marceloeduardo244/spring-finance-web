import React from 'react'

import Login from '../views/login'
import Home from '../views/home'
import CadastroUsuario from '../views/createUser'
import ConsultaLancamentos from '../views/lancamentos/get-appointment'
import CadastroLancamentos from '../views/lancamentos/create-appointment'
import LandingPage from '../views/landingPage'
import { AuthConsumer } from './authenticationProvider'

import { Route, Switch, BrowserRouter, Redirect } from 'react-router-dom'

function RotaAutenticada( { component: Component, isAuthenticatedUser, ...props } ){
    return (
        <Route exact {...props} render={ (componentProps) => {
            if(isAuthenticatedUser){
                return (
                    <Component {...componentProps} />
                )
            }else{
                return(
                    <Redirect to={ {pathname : '/login', state : { from: componentProps.location } } } />
                )
            }
        }}  />
    )
}

function Rotas(props){
    return (
        <BrowserRouter>
            <Switch>
                <Route exact path="/" component={LandingPage} />
                <Route exact path="/login" component={Login} />
                <Route exact path="/create-user" component={CadastroUsuario} />
                
                <RotaAutenticada isAuthenticatedUser={props.isAuthenticatedUser} path="/home" component={Home} />
                <RotaAutenticada isAuthenticatedUser={props.isAuthenticatedUser} path="/get-appointment" component={ConsultaLancamentos} />
                <RotaAutenticada isAuthenticatedUser={props.isAuthenticatedUser} path="/create-appointment/:id?" component={CadastroLancamentos} />
            </Switch>
        </BrowserRouter>
    )
}

export default () => (
    <AuthConsumer>
        { (context) => (<Rotas isAuthenticatedUser={context.isAuthenticated} />) }
    </AuthConsumer>
)