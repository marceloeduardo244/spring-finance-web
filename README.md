![image](https://img-c.udemycdn.com/course/480x270/2365916_6310.jpg)


# 🚀 Spring Finance Web

Bem-vindo(a). Este é o Spring Finance Web!

O objetivo desta aplicação integrar de forma visual as rotinas do backend go-finance (https://gitlab.com/marceloeduardo244/go-finance), com ferramentas para o controle financeiro que facilitam o seu dia-a-dia.

# 🧠 Contexto

Tecnologias utilizadas neste projeto:
-  React.JS
-  Javascript
-  Axios
-  Bootstrap
-  Docker
-  Kubernetes
-  Pipeline End to End
    - Na pipeline temos 2 jobs
        - run_build_test para testar a fase de build da aplicação
        - deploy_batch_docker para fazer o build e atualizar a imagem no docker-hub

## 📋 Instruções para subir a aplicação utilizando Docker-compose

É necessário ter o Docker e Docker-compose instalado e rodando na sua maquina!

- Faça clone do projeto
- Na raiz do projeto execute os comandos abaixo:
- docker-compose pull
- docker-compose up -d
- Vc terá 9 novos container rodando na sua maquina.
- O backend rodando por padrão na porta localhost:8080, do Postgres na porta localhost:5432, o pgadmin na porta localhost:80 e o Spring Finance Web rodando na porta 3000.
- Agora vc terá tbm o servidor proxy da aplicação, com os seguintes endereços
    - 80 - frontend
    - 9090 - backend
    - 9091 - pgadmin
    - 9092 - postgres
- Abaixo nesta doc tem a documentação das telas.

## 📋 Instruções para subir a aplicação utilizando o Kubernetes

É necessário ter o Docker e Docker-compose e Kubernets instalado e rodando na sua maquina!

- Faça clone do projeto
- Na raiz do projeto execute os comandos abaixo:
- docker-compose pull
- cd devops
- kubectl apply -f ./kubernetes
- Vc terá 9 novos container rodando na sua maquina.
- O backend rodando por padrão na porta localhost:30002, o Postgres poderá ser acessado via pgadmin na porta localhost:30003 e o Spring Finance Web na porta localhost:30004.
- Agora vc terá tbm o servidor proxy da aplicação, com os seguintes endereços
    - 80 - frontend
    - 9090 - backend
    - 9091 - pgadmin
- Para rodar o servidor execute os comandos abaixo na raiz do projeto
    - cd ./kubernetes/proxy-server/config
    - Edite o arquivo nginx.conf modificando o valor IPdaMAQUINA pelo ip da maquina onde cluster kubernetes foi iniciado.
- Para subir o container rode o comando:
        - 	docker run --name spring-finance-proxy-kubernetes -v caminho_até_o_arquivo_nginx/nginx.conf:/etc/nginx/nginx.conf -p 80:80 -p 9090:9090 -p 9091:9091 -p 9092:9092 -d marcelosilva404/spring-finance-proxy:1.0
- Abaixo nesta doc tem a documentação das telas.

## ✔️ Telas da APP

Breve descrição abaixo e imagem sobre as telas:

# Telas ja finalizadas
- / | Landing page. (https://gitlab.com/marceloeduardo244/spring-finance-web/-/blob/main/docs/pages/landing_page.jpg)
- /login | Login. (https://gitlab.com/marceloeduardo244/spring-finance-web/-/blob/main/docs/pages/login_page.jpg)
- /create-user | Criar novo usuário. (https://gitlab.com/marceloeduardo244/spring-finance-web/-/blob/main/docs/pages/sign_in_page.jpg)
- /home | Home. (https://gitlab.com/marceloeduardo244/spring-finance-web/-/blob/main/docs/pages/home_page.jpg)
- /get-appointment | Buscar apontamentos. (https://gitlab.com/marceloeduardo244/spring-finance-web/-/blob/main/docs/pages/get_appointments_page.jpg)
- /create-appointment | Criar apontamento. (https://gitlab.com/marceloeduardo244/spring-finance-web/-/blob/main/docs/pages/create_appointment_page.jpg)
- /create-appointment/:id | Editar apontamento. (https://gitlab.com/marceloeduardo244/spring-finance-web/-/blob/main/docs/pages/update_appointment_page.jpg)

# Video da aplicação
- Utilizando o sistema (https://gitlab.com/marceloeduardo244/spring-finance-web/-/blob/main/docs/utilizando_o_sistema.mp4)

# Backend da Aplicação
- https://gitlab.com/marceloeduardo244/spring-finance

# Integração completa da aplicação
- https://gitlab.com/marceloeduardo244/spring-finance-integrado

Made with 💜 at OliveiraDev 