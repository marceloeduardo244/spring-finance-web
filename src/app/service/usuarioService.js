import ApiService from '../apiservice'

import ValidationError from '../exception/validationError'

class UsuarioService extends ApiService {

    constructor(){
        super('/api/user')
    }

    autehticate(credenciais){
        return this.post('/auth', credenciais)
    }

    getBalanceById(id){
        return this.get(`/${id}/balance`);
    }

    save(user){
        return this.post('', user);
    }

    validate(user){
        const erros = []

        if(!user.name){
            erros.push('O campo Nome é obrigatório.')
        }

        if(!user.email){
            erros.push('O campo Email é obrigatório.')
        }else if( !user.email.match(/^[a-z0-9.]+@[a-z0-9]+\.[a-z]/) ){
            erros.push('Informe um Email válido.')
        }

        if(!user.password || !user.confirmPassword){
            erros.push('Digite a senha 2x.')
        }else if( user.password !== user.confirmPassword ){
            erros.push('As senhas não batem.')
        }        

        if(erros && erros.length > 0){
            throw new ValidationError(erros);
        }
    }

}

export default UsuarioService;