import React from 'react'

import { withRouter } from 'react-router-dom'
import Card from '../components/card'
import FormGroup from '../components/form-group'

import UsuarioService from '../app/service/usuarioService'
import { successMessage, errorMessage } from '../components/toastr'

class CadastroUsuario extends React.Component{

    state = {
        name : '',
        email: '', 
        password: '',
        confirmPassword : ''
    }

    constructor(){
        super();
        this.service = new UsuarioService();
    }

    create = () => {

        const {name, email, password, confirmPassword } = this.state        
        const user = {name,  email, password, confirmPassword }

        try{
            console.log(user)
            this.service.validate(user);
        }catch(error){
            console.log(error)
            const msgs = error.messages;
            msgs.forEach(msg => errorMessage(msg));
            return false;
        }

        const userToSave = {name,  email, password}

        this.service.save(userToSave)
            .then( response => {
                successMessage('Usuário cadastrado com sucesso! Faça o login para acessar o sistema.')
                this.props.history.push('/login')
            }).catch(error => {
                errorMessage(error.response.data)
            })
    }

    cancel = () => {
        this.props.history.push('/login')
    }

    render(){
        return (
            <Card title="Cadastro de Usuário">
                <div className="row">
                    <div className="col-lg-12">
                        <div className="bs-component">
                            <FormGroup label="Nome: *" htmlFor="inputNome">
                                <input type="text" 
                                       id="inputNome" 
                                       className="form-control"
                                       name="nome"
                                       onChange={e => this.setState({name: e.target.value})} />
                            </FormGroup>
                            <FormGroup label="Email: *" htmlFor="inputEmail">
                                <input type="email" 
                                       id="inputEmail"
                                       className="form-control"
                                       name="email"
                                       onChange={e => this.setState({email: e.target.value})} />
                            </FormGroup>
                            <FormGroup label="Senha: *" htmlFor="inputSenha">
                                <input type="password" 
                                       id="inputSenha"
                                       className="form-control"
                                       name="senha"
                                       onChange={e => this.setState({password: e.target.value})} />
                            </FormGroup>
                            <FormGroup label="Repita a Senha: *" htmlFor="inputRepitaSenha">
                                <input type="password" 
                                       id="inputRepitaSenha"
                                       className="form-control"
                                       name="senha"
                                       onChange={e => this.setState({confirmPassword: e.target.value})} />
                            </FormGroup>
                            <button onClick={this.create} type="button" className="btn btn-success">
                                <i className="pi pi-save"></i> Salvar
                            </button>
                            <button onClick={this.cancel} type="button" className="btn btn-danger">
                                <i className="pi pi-times"></i> Cancelar
                            </button>
                        </div>
                    </div>
                </div>
            </Card>
        )
    }
}

export default withRouter(CadastroUsuario)